# Collections

## SET

### Create table
```
CREATE TABLE cycling (
    id int PRIMARY KEY,
    firstname text,
    lastname text,
    nation text,
    carrier_teams set<text>,
    last_update timestamp
);
```
### Popis tabulky
```
DESC TABLE cycling;
```
### Insert data (v prvním řádku je dvakrát 'Individual' -> nevezme ho)
```
INSERT INTO cycling (id, firstname, lastname, nation, last_update, carrier_teams)
    VALUES (1, 'Eddy', 'Merckx', 'Belgium', toTimeStamp(now()), {'Individual', 'Faema', 'Molteni', 'Individual'});
INSERT INTO cycling (id, firstname, lastname, nation, last_update, carrier_teams)
    VALUES (2, 'Sean', 'Kelly', 'Ireland', toTimeStamp(now()), {'Individual', '	Kas'});
INSERT INTO cycling (id, firstname, lastname, nation, last_update, carrier_teams)
    VALUES (3, 'Gino', 'Bartali', 'Spain', toTimeStamp(now()), {'Legnano', 'Legnano - Pirelli', 'Bartali'});
```
### Select dat podle pořadí
```
SELECT id, firstname, lastname, nation, carrier_teams, last_update FROM cycling;
```
### Přidání itemu do týmu
```
UPDATE cycling SET carrier_teams = carrier_teams + {'Festina - Lotus'}, last_update = toTimeStamp(now()) WHERE id = 2;
```
### Indexace (vyhledávání)
```
CREATE INDEX team_idx ON cycling (carrier_teams);
```
```
SELECT id, firstname, lastname, nation, carrier_teams FROM cycling WHERE carrier_teams CONTAINS 'Individual';
```

## LIST

### Přidání sloupce pro výhry v malých, středních a velkých závodech (lokální, republikové, světové)
```
ALTER TABLE cycling ADD wins list<int>;
```
### Přidání výher
```
UPDATE cycling SET wins = [48, 8, 3], last_update = toTimeStamp(now()) WHERE id = 1;
UPDATE cycling SET wins = [25, 7, 1], last_update = toTimeStamp(now()) WHERE id = 2;
UPDATE cycling SET wins = [3, 3, 0], last_update = toTimeStamp(now()) WHERE id = 3;
```
### Select dat
```
SELECT id, firstname, lastname, nation, wins, last_update FROM cycling;
```
### Indexace (vyhledávání)
```
CREATE INDEX wins_idx ON cycling (wins);
```
```
SELECT id, firstname, lastname, nation, wins FROM cycling WHERE wins CONTAINS 0;
```
## MAP 

### Přidání sloupce pro týmy podle ročníku
```
ALTER TABLE cycling ADD teams_by_year map<int, text>;
```
### Přidání výher (v prvním řádku je přidán špatně dvakrát rok 1963 -> nevezme ho -> klíč je jedinečný)
```
UPDATE cycling SET teams_by_year = {1963: 'Individual', 1963: 'Faema', 1968: 'Faema', 1971: 'Molteni'}, last_update = toTimeStamp(now()) WHERE id = 1;
UPDATE cycling SET teams_by_year = {1974: 'Individual', 1986: 'Kas', 1992: 'Festina - Lotus'}, last_update = toTimeStamp(now()) WHERE id = 2;
UPDATE cycling SET teams_by_year = {1936: 'Legnano', 1946: 'Legnano - Pirelli', 1953: 'Bartali'}, last_update = toTimeStamp(now()) WHERE id = 3;
```
### Select dat
```
SELECT id, firstname, lastname, nation, carrier_teams, teams_by_year, last_update FROM cycling;
```
### Indexace (vyhledávání)
#### KEYS
```
CREATE INDEX years1_idx ON cycling (KEYS(teams_by_year));
```
```
SELECT id, firstname, lastname, nation, teams_by_year FROM cycling WHERE teams_by_year CONTAINS KEY 1974;
```
#### ENTRIES
```
CREATE INDEX years2_idx ON cycling (ENTRIES(teams_by_year));
```
```
SELECT id, firstname, lastname, nation, teams_by_year FROM cycling WHERE teams_by_year[1974] = 'Individual';
```
#### VALUES
```
CREATE INDEX years3_idx ON cycling (VALUES(teams_by_year));
```
```
SELECT id, firstname, lastname, nation, teams_by_year FROM cycling WHERE teams_by_year CONTAINS 'Individual';
```

## FROZEN + UDT

### Vytvoření UDT (uživatelsky definovaný typ = user defined type)
```
CREATE TYPE address_type (
    number text,
    street text,
    city text
);
```
### Popis UDT
```
DESC TYPE address_type;
```
### Přidání sloupce s adresami
```
ALTER TABLE cycling ADD address frozen<address_type>;
```
### Přidání adres (Co se nachází na těchto adresách ve skutečnosti?)
```
UPDATE cycling SET address = {number: '1558/21', street: 'Jihlavská', city: 'Praha'} WHERE id = 1;
UPDATE cycling SET address = {number: '529/5', street: 'Karmelitská', city: 'Praha'} WHERE id = 2;
UPDATE cycling SET address = {number: '109/33', street: 'Mírové náměstí', city: 'Ústí nad Labem'} WHERE id = 3;
```
### Select dat
```
SELECT id, firstname, lastname, nation, address, last_update FROM cycling;
```
### Proč je to ale FROZEN?

Nejde změnit jen část "pole". Musí se nastavit jako celek. Např. adresy, kde na sobě jednotlivé části závisí.
Toto platí i pro set, list i map.
