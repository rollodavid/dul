# MDX

## Př 1
```
SELECT
	{[Dim Product].[Properties].[Color]} ON COLUMNS,
	{[Dim Customer].[Geography].[Country]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 2 - PREVMEMBER
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue].PREVMEMBER} ON COLUMNS,
	{[Dim Customer].[Geography].[Country]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 3 - NEXTMEMBER
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue].NEXTMEMBER} ON COLUMNS,
	{[Dim Customer].[Geography].[Country]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 4 - LEAD (skok o N prvků dopředu)
Pokud použijeme zápornou hodnotu, budeme postupovat dozadu.
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue].LEAD(2)} ON COLUMNS,
	{[Dim Customer].[Geography].[Country]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 5 - LAG (skok o N prvků dozadu)
Pokud použijeme zápornou hodnotu, budeme postupovat dopředu.
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue].LAG(1)} ON COLUMNS,
	{[Dim Customer].[Geography].[Country]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 6 - MEMBERS
### Bez MEMBERS
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue]} ON COLUMNS,
	{[Order Date].[Calendar Year]} ON ROWS
FROM [Analysis Services Tutorial]
```
### S MEMBERS
```
SELECT
	{[Dim Product].[Properties].[Color].[Blue]} ON COLUMNS,
	{[Order Date].[Calendar][Year].MEMBERS} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 7 - WHERE
```
SELECT
	{[Dim Product].[Product Key]} ON COLUMNS,
	{[Order Date].[Calendar].[Year]} ON ROWS
FROM [Analysis Services Tutorial]
```
```
SELECT
	{[Dim Product].[Product Key]} ON COLUMNS,
	{[Order Date].[Calendar].[Year]} ON ROWS
FROM [Analysis Services Tutorial]
WHERE {[Dim Product].[Properties].[Color].[Blue]}
```

## Př 8 - NON EMPTY
### Bez NON EMPTY
```
SELECT 
	{[Dim Product].[Properties].[Color]} ON COLUMNS,
	{[Order Date].[Calendar].[Year]} ON ROWS
FROM [Analysis Services Tutorial]
```
### S NON EMPTY
```
SELECT 
	NON EMPTY
	{[Dim Product].[Properties].[Color]} ON COLUMNS,
	NON EMPTY
	{[Order Date].[Calendar].[Year]} ON ROWS
FROM [Analysis Services Tutorial]
```

## Př 9 - CROSSJOIN
```
SELECT 
	CROSSJOIN(
		{[Dim Customer].[Geography].[Country].[France],
			[Dim Customer].[Geography].[Country].[Germany]},
		{[Order Date].[Calendar].[Year].[2011] :
			[Order Date].[Calendar].[Year].[2013]})
		ON COLUMNS,
	{[Dim Product].[Color].[Black], [Dim Product].[Color].[Blue]} ON ROWS
FROM [Analysis Services Tutorial]
```
```
SELECT 
	CROSSJOIN(
		{[Dim Customer].[Geography].[Country].[France],
			[Dim Customer].[Geography].[Country].[Germany]},
		{[Order Date].[Calendar].[Year].[2011] :
			[Order Date].[Calendar].[Year].[2013]})
		ON COLUMNS,
	CROSSJOIN(
		{[Dim Product].[Color].[Black],
			[Dim Product].[Color].[Blue]},
		{[Dim Product].[Size].[50] :
			[Dim Product].[Size].[60]})
	ON ROWS
FROM [Analysis Services Tutorial]
```
